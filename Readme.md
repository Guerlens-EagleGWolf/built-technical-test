# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Java version
  `Java-17`

* System dependencies
  
  `docker & docker compose` [ https://www.simplilearn.com/tutorials/docker-tutorial/how-to-install-docker-on-ubuntu]

## Instructions
### Start everything effortless
```
./restartContainer.sh
```
go to [http://localhost:8080](http://localhost:8080)

### GOD-MOD Destroy everything effortless (DESTROY ALL DOCKER RELATED PODS, IMAGES, VOLUMES)
```
./destroy.sh
```

### Restart everything effortless
```
./restartContainer.sh
```

### Yop, you guess it :: Stop everything effortless
```
./stopContainer.sh
```

## Available CRUD paths

go to [swagger](http://localhost:8080/swagger-ui/index.html#/)

## Some other commands

* Database connection
```
MYSQL_HOST: localhost:3306
MYSQL_USER: root
MYSQL_ROOT_PASSWORD: password
```

* How to run any command in side the database 
```
cd springapp

docker ps
docker exec -it {database_CONTAINER_ID} bash
mysql -u root -p
```

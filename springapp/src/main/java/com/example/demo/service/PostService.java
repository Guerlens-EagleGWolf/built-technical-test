package com.example.demo.service;

import com.example.demo.entities.Post;
import com.example.demo.repository.PostRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class PostService {

    @Autowired
    private PostRepository postRepository;
    
    public List<Post> getPosts(){
        return postRepository.findAll();
    } 

    public Post findById(Long id) {
        return postRepository.findById(id).orElse(null);
    }

    public Post createPost(Post post) {
        return postRepository.save(post);
    }

    public Post updatePost(Post post) {
        
        Post oldPost = findById(post.getId());
        // oldPost.setName(post.getName());
        return createPost(oldPost);
    }

    public void deleteById(Long id) {
        postRepository.deleteById(id);
    }

    public void deleteAllPost() {
        postRepository.deleteAll();
    }
}

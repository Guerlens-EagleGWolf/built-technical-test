package com.example.demo.entities;

public enum Categories {
  GENERAL("General"),
  TECHNOLOGY("Technology"),
  RANDOM("Random");

  public final String label;

  private Categories(String label) {
      this.label = label;
  }
}

package com.example.demo.controller;

import com.example.demo.entities.Post;
import java.util.List;
import com.example.demo.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("posts")
public class PostController {

  @Autowired
  private PostService postService;

  @GetMapping()
  public List<Post> getPosts(){
    return postService.getPosts();
  }
  
    @GetMapping("/{id}")
  public Post getPosts(@PathVariable Long id){
    return postService.findById(id);
  }
  
  @PostMapping()
  public Post createPost(@RequestBody Post post) {
    return postService.createPost(post);
  }
  
  @PutMapping("/{id}")
    public Post updatePost(@RequestBody Post post, @PathVariable Long id) {
    return postService.updatePost(post);
  }

  @DeleteMapping()
    public void updatePost() {
    postService.deleteAllPost();
  }
    
   @DeleteMapping("/{id}")
    public void updatePost(@PathVariable Long id) {
    postService.deleteById(id);
  }

}

package com.example.demo.controller;

import com.example.demo.entities.Category;
import java.util.List;
import com.example.demo.service.CategoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("categories")
public class CategoryController {

  @Autowired
  private CategoryService categoryService;

  @GetMapping()
  public List<Category> getCategories(){
    return  categoryService.getCategories();
  }


}

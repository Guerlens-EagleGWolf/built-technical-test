CREATE TABLE post (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255),
    contents TEXT,
    timestamp TIMESTAMP,
    category_id BIGINT,
    FOREIGN KEY (category_id) REFERENCES category(id)
);
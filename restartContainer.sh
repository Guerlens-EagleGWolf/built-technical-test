#!/bin/bash

docker-compose down
docker rmi -f springapp-web
cd springapp
docker-compose up
